using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A)) 
        {
            transform.Translate(speed * Vector3.left);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(speed * Vector3.right);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(speed * Vector3.back);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(speed * Vector3.forward);
        }
    }
}
